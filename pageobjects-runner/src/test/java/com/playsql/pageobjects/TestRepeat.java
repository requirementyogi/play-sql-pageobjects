package com.playsql.pageobjects;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.playsql.pageobjects.junit.AbstractPageObjectsTest;
import com.playsql.pageobjects.junit.POSuiteRunner;
import com.playsql.pageobjects.junit.POSuiteTestedProducts;
import com.playsql.pageobjects.junit.POTestedProduct;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(POSuiteRunner.class)
@POSuiteTestedProducts({
        "confluence1, example",
        "confluence2, example",
        "confluence3, example",
        "confluence4, example"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRepeat extends AbstractPageObjectsTest {

    private final List<String> instanceIdsForThisIteration;

    public TestRepeat (List<String> instanceIds) {
        instanceIdsForThisIteration = instanceIds;
    }

    private static List<String> runs;

    @BeforeClass
    public static void reset() {
        runs = Lists.newArrayList();
    }

    @Test
    public void test1AlwaysRun() {
        assertThat(product(), notNullValue());
        assertThat(product().getProduct(), is("confluence"));
        runs.add("always");
    }

    @POTestedProduct("confluence1")
    @Test
    public void test2() {
        runs.add("confluence1");
    }

    @POTestedProduct(not = "confluence1")
    @Test
    public void test3() {
        runs.add("not confluence1");
    }

    @Test
    public void test9CorrectlyRun() {
        assertThat(runs, hasItems("always"));
        if (instanceIdsForThisIteration.contains("confluence1")) {
            assertThat(runs, hasItems("confluence1"));
            assertThat(runs, not(hasItems("not confluence1")));
        } else {
            assertThat(runs, not(hasItems("confluence1")));
            assertThat(runs, hasItems("not confluence1"));
        }
    }
}
