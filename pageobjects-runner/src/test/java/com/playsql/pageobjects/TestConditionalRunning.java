package com.playsql.pageobjects;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.playsql.pageobjects.junit.AbstractPageObjectsTest;
import com.playsql.pageobjects.junit.POTestedProduct;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestConditionalRunning extends AbstractPageObjectsTest {

    private static List<String> runs = Lists.newArrayList();

    @Test
    public void test1AlwaysRun() {
        // "confluence1" is declared as instance.default. Therefore we only get one product,
        // and only tests which include it or don't exclude it will run.
        // As a bonus we're also testing the abstract class works as intended.
        assertThat(product(), notNullValue());
        assertThat(product().getProduct(), is("confluence"));
        runs.add("always");
    }

    @POTestedProduct("confluence1")
    @Test
    public void test2IncludeExisting() {
        runs.add("confluence1");
    }

    @POTestedProduct("confluence2")
    @Test
    public void test3IncludeNonExisting() {
        runs.add("confluence2");
    }

    @POTestedProduct(not = "confluence1")
    @Test
    public void test4ExcludeExisting() {
        runs.add("not confluence1");
    }

    @POTestedProduct(not = "confluence2")
    @Test
    public void test5ExcludeNonExisting() {
        runs.add("not confluence2");
    }

    @Test
    public void test9CorrectlyRun() {
        assertThat(runs, hasItems("always"));
        assertThat(runs, hasItems("confluence1", "not confluence2"));
        assertThat(runs, not(hasItems("confluence2", "not confluence1")));
    }
}
