package com.playsql.pageobjects;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.pages.CompositionAcrossProducts;
import com.playsql.pageobjects.pages.ExamplePage;
import com.playsql.pageobjects.pages.ExamplePageVersion;
import com.playsql.pageobjects.pages.ParentPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class TestPageObjectsRunner {

    private static PORunner runner;

    @BeforeClass
    public static void setUp() {
        runner = new PORunner("/pageobjects-runner.properties");
    }

	@AfterClass
	public static void closeBrowser(){
		 runner.stop();
	 }

    @Test
    public void testPageInheritance() {

        assertEquals(ExamplePageVersion.V431.class, runner.getInstance("confluence2").getCorrectClass(ExamplePageVersion.V431.class));
        assertEquals(ExamplePageVersion.V51.class, runner.getInstance("confluence3").getCorrectClass(ExamplePageVersion.V431.class));
        assertEquals(ExamplePageVersion.V6Cloud.class, runner.getInstance("confluence4").getCorrectClass(ExamplePageVersion.V431.class));
        assertEquals(ExamplePageVersion.V51.class, runner.getInstance("confluence5").getCorrectClass(ExamplePageVersion.V431.class));

        try {
            runner.getInstance("confluence1").getCorrectClass(ExamplePageVersion.V431.class);
            assertTrue("An exception should be thrown to explain that there's no pageobject for the instance 4.3.1", false);
        } catch (IllegalArgumentException iae) {
            // Exception expected
        }
    }

    @Test
    public void testCompareVersions() {
        assertEquals(0, TestedProduct.compareVersions(null, null));
        assertEquals(0, TestedProduct.compareVersions(null, ""));
        assertEquals(0, TestedProduct.compareVersions("", ""));
        assertEquals(-1, TestedProduct.compareVersions(null, "a"));
        assertEquals(1, TestedProduct.compareVersions("b", null));

        assertEquals(0, TestedProduct.compareVersions("1", "1"));
        assertEquals(0, TestedProduct.compareVersions("5.4.3.2", "5.4.3.2"));
        assertEquals(-1, TestedProduct.compareVersions("1.2", "1.3"));
        assertEquals(-1, TestedProduct.compareVersions("1.2", "1.2.3"));
        assertEquals(-1, TestedProduct.compareVersions("1.1.9", "1.2"));
        assertEquals(-1, TestedProduct.compareVersions("1.1.9", "1.2.0"));
        assertEquals(1, TestedProduct.compareVersions("1.2.0.1", "1.2.0"));

        assertEquals(1, TestedProduct.compareVersions("1.10.100.1010", "1.10.100.1000"));
        assertEquals(1, TestedProduct.compareVersions("1.10.100.1010", "1.10.100.10"));

        try {
            TestedProduct.compareVersions("1.10..1010", "1.10.100.10");
            assertTrue("An exception should have been thrown", false);
        } catch (RuntimeException re) {
            // Exception expected
        }
        // The shorter version number wins
        assertEquals(-1, TestedProduct.compareVersions("1.", "1.0"));
    }

    @Test
    public void testElementTypes() {
        runner.stop().start();
        ExamplePage page = runner.getInstance("example").goTo(ExamplePage.class);
        assertEquals("Example Domain", page.getTitle());
        List<String> paragraphs = page.getParagraphs();
        assertEquals(2, paragraphs.size());
        paragraphs = page.getParagraphs2();
        assertEquals(2, paragraphs.size());
    }

    @Test
    public void testComposition() {
        runner.stop().start();
        ParentPage page = runner.getInstance("example").goTo(ParentPage.class);
        assertThat(page.page, notNullValue());
        assertThat(page.page.body, notNullValue());
    }

    @Test
    public void testCompositionAcrossProducts() {
        File folder = new File("src/test/resources/html");
        if (!folder.exists()) throw new RuntimeException("Couldn't find the test folder src/test/resources/html from current folder: " + new File(".").getAbsolutePath());

        runner.stop().start();
        runner.overrideProperty("instance.local-a.product", "a");
        runner.overrideProperty("instance.local-b.product", "b");
        runner.overrideProperty("instance.local-a.baseurl", "file://" + folder.getAbsolutePath());
        runner.overrideProperty("instance.local-b.baseurl", "file://" + folder.getAbsolutePath());
        TestedProduct productA = runner.getInstance("local-a");
        TestedProduct productB = runner.getInstance("local-b");
        CompositionAcrossProducts.ParentPage page = productA.goTo(CompositionAcrossProducts.ParentPage.class);

        assertThat(page.getTestedProduct().getProduct(), is("a"));
        assertThat(page.subPage.getTestedProduct().getProduct(), is("b"));

        assertThat(page.getTestString(), is("PAGE1"));
        // The subpage still displays PAGE1 since we didn't specify we're in an iframe
        assertThat(page.subPage.getTestString(), is("PAGE1"));
    }

    @Test
    public void testCompositionAcrossIframes() {
        File folder = new File("src/test/resources/html");
        if (!folder.exists()) throw new RuntimeException("Couldn't find the test folder src/test/resources/html from current folder: " + new File(".").getAbsolutePath());

        runner.stop().start();
        runner.overrideProperty("instance.local-a.product", "a");
        runner.overrideProperty("instance.local-b.product", "b");
        runner.overrideProperty("instance.local-a.baseurl", "file://" + folder.getAbsolutePath());
        runner.overrideProperty("instance.local-b.baseurl", "file://" + folder.getAbsolutePath());
        TestedProduct productA = runner.getInstance("local-a");
        TestedProduct productB = runner.getInstance("local-b");
        CompositionAcrossProducts.ParentPageWithIframe page = productA.goTo(CompositionAcrossProducts.ParentPageWithIframe.class);

        assertThat(page.getTestedProduct().getProduct(), is("a"));
        assertThat(page.subPageInIframe.getTestedProduct().getProduct(), is("b"));

        assertThat(page.getTestString(), is("PAGE1"));
        assertThat(page.subPageInIframe.getTestString(), is("PAGE2"));
    }
}
