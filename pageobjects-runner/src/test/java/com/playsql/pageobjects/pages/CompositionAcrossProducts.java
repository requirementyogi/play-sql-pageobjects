package com.playsql.pageobjects.pages;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.TestedProduct;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;

public class CompositionAcrossProducts {


    @POPage(
            url = "/parentPage.html",
            product = "a"
    )
    public static class ParentPage extends AbstractPage {

        @POBy("")
        public SubPage subPage;

        @POBy(".ref1")
        private WebElement testString;

        public TestedProduct getTestedProduct() {
            return product;
        }

        public String getTestString() {
            return testString.getText();
        }
    }

    @POPage(
            url = "/parentPage.html",
            product = "a"
    )
    public static class ParentPageWithIframe extends AbstractPage {

        /** This command means all elements of the subpage will
         * relate to its iframe
         */
        @POBy(value = "", iframe = "iframe")
        public SubPage subPageInIframe;

        @POBy(value = ".ref1")
        private WebElement testString;

        public TestedProduct getTestedProduct() {
            return product;
        }

        public String getTestString() {
            return testString.getText();
        }
    }

    @POPage(
            url = "/childPage.html",
            product = "b"
    )
    public static class SubPage extends AbstractPage {

        @POBy(".ref1")
        private WebElement testString;

        public TestedProduct getTestedProduct() {
            return product;
        }

        public String getTestString() {
            return testString.getText();
        }
    }

}
