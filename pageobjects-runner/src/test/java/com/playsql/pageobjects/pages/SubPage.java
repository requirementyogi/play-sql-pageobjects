package com.playsql.pageobjects.pages;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.TestedProduct;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;

@POPage(product = "example")
public class SubPage extends AbstractPage {

    @POBy("body")
    public WebElement body;

    public TestedProduct getTestedProduct() {
        return product;
    }

}
