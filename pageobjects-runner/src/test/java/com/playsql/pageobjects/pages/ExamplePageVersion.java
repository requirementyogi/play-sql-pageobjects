package com.playsql.pageobjects.pages;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.AbstractPage;

public class ExamplePageVersion {

    @POPage(
            product = "confluence",
            version = "4.3.1"
    )
    public static class V431 extends AbstractPage {
    }

    @POPage(
            product = "confluence",
            version = "5.1"
    )
    public static class V51 extends V431 {

    }

    @POPage(
            product = "confluence",
            version = "5.5",
            variant = "cloud"
    )
    public static class V55Cloud extends V51 {

    }

    @POPage(
            product = "confluence",
            version = "6",
            variant = "cloud"
    )
    public static class V6Cloud extends V51 {

    }
}
