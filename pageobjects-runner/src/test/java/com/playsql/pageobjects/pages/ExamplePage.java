package com.playsql.pageobjects.pages;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.WebElement;

import java.util.List;

@POPage(
        url = "/"
)
public class ExamplePage extends AbstractPage {

    @POBy("h1")
    private WebElement title;

    @POBy("div p")
    private List<WebElement> paragraphs;

    @POBy("div p")
    private Iterable<WebElement> paragraphs2;

    public String getTitle() {
        return title.getText();
    }

    public List<String> getParagraphs() {
        return Lists.transform(paragraphs, new Function<WebElement, String>() {
            @Override
            public String apply(WebElement input) {
                return input.getText();
            }
        });
    }

    public List<String> getParagraphs2() {
        return Lists.newArrayList(Iterables.transform(paragraphs2, new Function<WebElement, String>() {
            @Override
            public String apply(WebElement input) {
                return input.getText();
            }
        }));
    }

}
