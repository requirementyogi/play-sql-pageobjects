package com.playsql.pageobjects.elements;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.PORunner;
import com.playsql.pageobjects.TestedProduct;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;

public abstract class AbstractPage {

    @POBy("body")
    protected WebElement body;

    @Inject
    protected TestedProduct product;

    @Inject
    protected PORunner runner;

    /** Return the {@link POPage#url()} annotation.
     * If the url depends on instance members, please override this method.
     * @return the url (without the base url)
     */
    public String getUrl() {
        POPage annotation = this.getClass().getAnnotation(POPage.class);
        if (annotation == null) {
            throw new IllegalStateException(this.getClass().getCanonicalName()
                    + " is not annotated by a "
                    + POPage.class.getCanonicalName());
        }
        return annotation.url();
    }


}
