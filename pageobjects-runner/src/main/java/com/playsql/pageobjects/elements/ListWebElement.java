package com.playsql.pageobjects.elements;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class ListWebElement implements InvocationHandler {

    private List<WebElement> delegate = null;
    private final RemoteWebDriver driver;
    private By annotation;

    public ListWebElement(RemoteWebDriver driver, By annotation) {
        this.driver = driver;
        this.annotation = annotation;
    }

    public List<WebElement> init() {
        if (delegate == null) {
            delegate = driver.findElements(annotation);
        }
        return delegate;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            // Execute the method on the delegate list of WebElements
            return method.invoke(init(), args);
        } catch (InvocationTargetException e) {
          throw e.getTargetException();
        } catch (Exception e) {
          throw e;
        }
    }
}
