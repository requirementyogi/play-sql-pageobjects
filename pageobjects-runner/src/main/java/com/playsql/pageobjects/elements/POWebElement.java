package com.playsql.pageobjects.elements;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.PORunner;
import org.openqa.selenium.*;

import java.util.List;
import java.util.Objects;

public class POWebElement implements WebElement {


    private WebElement delegate = null;
    private final PORunner runner;
    private By annotation;
    private String iframe;

    public POWebElement(PORunner runner, By annotation, String iframe) {
        this.runner = runner;
        this.annotation = annotation;
        this.iframe = iframe;
    }

    private WebElement init() {
        runner.checkFrame(iframe);
        if (delegate == null) {
            delegate = runner.getDriver().findElement(annotation);
        }
        return delegate;
    }

    public By getByAnnotation() {
        return annotation;
    }

    public String getIframe() {
        return iframe;
    }

    /**
     * The underlying WebElement is cached the first time a method is called. This method
     * resets the underlying WebElement, with the additional feature that it immediately
     * ensures there's an element, and ensures it uses the visible one (e.g. if there are
     * two such elements on the page, returns the visible one, or the first
     * one even if not visible, or throws an exception if no such element).
     * @return
     */
    public WebElement reset() {
        delegate = null;
        runner.checkFrame(iframe);
        List<WebElement> candidates = runner.getDriver().findElements(annotation);
        for (WebElement candidate : candidates) {
            if (candidate.isDisplayed()) {
                delegate = candidate;
                break;
            }
        }
        if (delegate == null && !candidates.isEmpty()) {
            delegate = candidates.get(0);
        }
        return init();
    }

    @Override
    public void click() {
        init().click();
    }

    @Override
    public void submit() {
        init().submit();
    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {
        init().sendKeys(keysToSend);
    }

    @Override
    public void clear() {
        init().clear();
    }

    @Override
    public String getTagName() {
        return init().getTagName();
    }

    @Override
    public String getAttribute(String name) {
        return init().getAttribute(name);
    }
    @Override
    public String getText() {
        return init().getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return init().findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return init().findElement(by);
    }

    @Override
    public boolean isDisplayed() {
        // We need to catch the exception because findElement() will wait until the
        // element is... displayed. Therefore I don't really understand how the native
        // isDisplayed() is supposed to work. At least we return true and false in the
        // default situations.
        try {
            return init().isDisplayed();
        } catch (NoSuchElementException nsee) {
            return false;
        }
    }

    @Override
    public boolean isSelected() {
        return init().isSelected();
    }

    @Override
    public boolean isEnabled() {
        return init().isEnabled();
    }

    @Override
    public Point getLocation() {
        return init().getLocation();
    }

    @Override
    public Dimension getSize() {
        return init().getSize();
    }

    @Override
    public String getCssValue(String propertyName) {
        return init().getCssValue(propertyName);
    }

    @Override
    public Rectangle getRect() {
        return init().getRect();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return init().getScreenshotAs(outputType);
    }

    @Override
    public String toString() {
        return String.format("POWebElement{%s, iframe:%s}",
                Objects.toString(annotation),
                iframe);
    }
}
