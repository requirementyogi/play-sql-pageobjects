package com.playsql.pageobjects;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.annotations.POPackage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Objects.equal;

public class TestedProduct {
    private PORunner runner;
    private final String instanceId;
    private final String product;
    private final String variant;
    private final String version;
    private final String baseUrl;

	private static ObjectMapper MAPPER = new ObjectMapper();

    public TestedProduct(PORunner runner, String instanceId, String product, String variant, String version, String baseUrl) {
        this.runner = runner;
        this.instanceId = instanceId;
        this.product = product;
        this.variant = variant;
        this.version = version;
        this.baseUrl = baseUrl;
    }

    public String getProperty(String key, String defaultValue) {
        return runner.getProperty("instance." + instanceId + "." + key, defaultValue);
    }

    /**
     * Finds the correct class for the product's version, instantiates it, and injects its members.
     * @param pageClass the class
     * @param constructorArguments arguments for the page's constructor
     * @return an instance of a child class of pageClass
     */
    public <P extends AbstractPage> P bind(Class<P> pageClass, Object... constructorArguments) {
        return bind(pageClass, null, constructorArguments);
    }

    /**
     * Finds the correct class for the product's version, instantiates it, and injects its members.
     * @param pageClass the class
     * @param scope all elements of the page will be bound within the scope of this annotation. Notably, the scope can
     *              specify an iframe, and all members of the page will be tied to that iframe.
     * @param constructorArguments arguments for the page's constructor
     * @return an instance of a child class of pageClass
     */
    protected <P extends AbstractPage> P bind(Class<P> pageClass, POBy scope, Object... constructorArguments) {
        P page = getInstance(pageClass, constructorArguments);
        return runner.inject(page, this, scope);
    }

    /**
     * Go to a page of the application
     * @param pageClass the page. The framework will choose which implementation to use depending
     *                  on the product's version.
     * @param constructorArguments some arguments to pass to the constructor of the object.
     *                             It's most suitable when the object depends on parameters.
     */
    public <P extends AbstractPage> P goTo(Class<P> pageClass, Object... constructorArguments) {
        P page = getInstance(pageClass, constructorArguments);
        runner.navigateTo(baseUrl + page.getUrl());
        return runner.inject(page, this, null);
    }

    /**
     * Returns the class of the PageObject, after reading the annotation and taking the most suitable class.
     * @param pageClass the expect class (or a descendent)
     * @param <P>
     * @return
     */
    protected <P extends AbstractPage> Class<P> getCorrectClass(final Class<P> pageClass) {
        registerPageObjectClass(pageClass);
        Set<Map.Entry<Class<?>, POPage>> entries = PAGE_OBJECT_POOL.entrySet();
        Iterable<Map.Entry<Class<?>, POPage>> entries2 = Iterables.filter(entries, new Predicate<Map.Entry<Class<?>, POPage>>() {
            @Override
            public boolean apply(Map.Entry<Class<?>, POPage> input) {
                // We only accept pageobjects:
                // - of the same product (or unspecified)
                // - with older versions
                // - with empty variant or a variant equal to the TestedProduct's variant
                // - which can be assigned, therefore no parent classes
                if (!pageClass.isAssignableFrom(input.getKey()))
                    return false;
                POPage annotation = input.getValue();
                if (product != null && StringUtils.isNotBlank(annotation.product())
                        && !equal(product, annotation.product()))
                    return false;
                if (version != null && StringUtils.isNotBlank(annotation.version())
                        && compareVersions(version, annotation.version()) < 0)
                    return false;
                if (StringUtils.isNotBlank(annotation.variant())
                        && !equal(variant, annotation.variant()))
                    return false;
                return true;
            }
        });
        if (Iterables.isEmpty(entries2)) {
            throw new IllegalArgumentException("There is no " + pageClass.getName() + " page object for " + product + " v" + version + (StringUtils.isNotBlank(variant) ? " " + variant : "")
                    + ". Have you added it to " + pageClass.getPackage().getName().replace(".", "/") + "/package-info.java?");
        }
        Map.Entry<Class<?>, POPage> bestEntry = MOST_SUITABLE_PAGE_OBJECT.max(entries2);
        Class<P> pageObjectClass = (Class<P>) bestEntry.getKey();
        return pageObjectClass;
    }

    private final static Ordering<Map.Entry<Class<?>, POPage>> MOST_SUITABLE_PAGE_OBJECT = new Ordering<Map.Entry<Class<?>, POPage>>() {
        @Override
        public int compare(Map.Entry<Class<?>, POPage> left, Map.Entry<Class<?>, POPage> right) {
            POPage annotationLeft = left.getValue();
            POPage annotationRight = right.getValue();
            int versionComparison = compareVersions(annotationLeft.version(), annotationRight.version());
            if (versionComparison != 0) return versionComparison;
            if (annotationLeft.variant() != null && annotationRight.variant() == null) return 1;
            if (annotationLeft.variant() == null && annotationRight.variant() != null) return -1;
            // Same version, same variant. We take the lowest in the hierarchy, because the highest is older.
            boolean leftDescendsFromRight = left.getKey().isAssignableFrom(right.getKey());
            boolean rightDescendsFromLeft = left.getKey().isAssignableFrom(right.getKey());
            if (leftDescendsFromRight && !rightDescendsFromLeft) return -1;
            if (!leftDescendsFromRight && rightDescendsFromLeft) return 1;
            return 0;
        }
    };

    private final static Map<Class<?>, POPage> PAGE_OBJECT_POOL = Maps.newHashMap();

    private <P extends AbstractPage> void registerPageObjectClass(Class<P> pageClass) {
        if (!PAGE_OBJECT_POOL.containsKey(pageClass)) {
            Package classPackage = pageClass.getPackage();
            POPackage packageAnnotation = classPackage != null ? classPackage.getAnnotation(POPackage.class) : null;
            if (packageAnnotation == null) {
                String packageName = classPackage != null ? classPackage.getName() : "(root package)";
                StringBuilder message = new StringBuilder();
                message.append("The package ").append(packageName).append(" is not annotated with @POPackage. \n");
                message.append("Please create ").append(packageName.replace(".", "/")).append("/package-info.java with the contents: \n");
                message.append("@POPackage({ ... list of classes to declare as page objects ... })\n");
                message.append("package ").append(packageName).append(";\n(end of file)\n");
                throw new RuntimeException(message.toString());
            }
            if (packageAnnotation.value() != null) {
                for (Class<? extends AbstractPage> candidate : packageAnnotation.value()) {
                    POPage annotation = candidate.getAnnotation(POPage.class);
                    if (annotation == null) {
                        StringBuilder message = new StringBuilder();
                        message.append("The page ").append(pageClass.getSimpleName()).append(" is not annotated with @POPage.");
                        throw new RuntimeException(message.toString());
                    }
                    PAGE_OBJECT_POOL.put(candidate, annotation);
                }
            }

            Class<? super P> superclass = pageClass.getSuperclass();
            if (superclass != null
                    && AbstractPage.class.isAssignableFrom(superclass)
                    && superclass.isAnnotationPresent(POPage.class)) {
                registerPageObjectClass((Class<? extends AbstractPage>) superclass);
            }
        }

        // Perform a check that the product is correct
        POPage annotation = pageClass.getAnnotation(POPage.class);
        if (StringUtils.isNotBlank(annotation.product()) && !equal(annotation.product(), product)) {
            StringBuilder message = new StringBuilder();
            message.append("The page ").append(pageClass.getSimpleName())
                    .append(" can't be registered because its product ")
                    .append(annotation.product()).append(" doesn't match the TestedProduct(").append(product).append(").");
            throw new RuntimeException(message.toString());
        }

    }

    /**
     * Compare two version numbers. Versions must be numbers separated by dots.
     * 1.2 < 1.2.3
     * 1.2 < 1.5
     * 1.2 < 1.10
     * 1.1.9 < 1.2.0
     *
     * Not accepted: "1.0-SNAPSHOT", "1.3.4.5.6.7.8"
     *
     * @return -1 if A < B, 0 if A == B, 1 if A > B
     */
    static int compareVersions(String versionA, String versionB) {
        if (StringUtils.isBlank(versionA))
            return StringUtils.isBlank(versionB) ? 0 : -1;
        if (StringUtils.isBlank(versionB))
            return 1;
        String[] versionAsplit = versionA.split("\\.");
        String[] versionBsplit = versionB.split("\\.");
        int i = 0;
        while (i < versionAsplit.length && i < versionBsplit.length) {
            if (StringUtils.isNumeric(versionAsplit[i]) && StringUtils.isNumeric(versionBsplit[i])) {
                int a = Integer.parseInt(versionAsplit[i], 10);
                int b = Integer.parseInt(versionBsplit[i], 10);
                if (a < b) return -1;
                if (a > b) return 1;
                // Otherwise continue looping through dotversions
            } else {
                throw new NotImplementedException("compareVersions is not implemented for non-numeric version numbers: " + versionA + ", " + versionB);
            }
            i++;
        }
        if (i < versionAsplit.length) {
            return 1;
        } else if (i < versionBsplit.length) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Find the right version of the page class and returns an instance of it
     * @param pageClass the page class. The annotation will be used to find the proper page class.
     * @param constructorArguments a few arguments to pass on to the constructor
     */
    private <P extends AbstractPage> P getInstance(Class<P> pageClass, Object... constructorArguments) {
        Class<P> correctClass = getCorrectClass(pageClass);
        try {
            return ConstructorUtils.invokeConstructor(correctClass, constructorArguments);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Use the annotation on the provided class to return an instance of the correct product.
     * @param scope all elements of the page will be bound within the scope of this annotation. Notably, the scope can
     *              specify an iframe, and all members of the page will be tied to that iframe.
     */
    public static <P extends AbstractPage> P getInstance(Class<? extends P> pageClass, Iterable<TestedProduct> products, POBy scope) {
        POPage annotation = pageClass.getAnnotation(POPage.class);
        if (annotation == null) throw new IllegalArgumentException("A class without @POPage annotation was requested: " + pageClass.getCanonicalName());
        String productName = annotation.product();
        for (TestedProduct product : products)
            if (equal(productName, product.getProduct()))
                return product.bind(pageClass, scope);

        // Display an error
        List<String> productNames = Lists.newArrayList();
        for (TestedProduct product : products)
            productNames.add(product.getProduct());
        throw new IllegalArgumentException("An instance of " + pageClass.getCanonicalName() + " was requested, but no " +
                "product with name " + productName + " was provided. " +
                "Provided products: " + StringUtils.join(productNames, ", "));
    }

    public WebTarget rest() {
        return runner.rest(baseUrl);
    }

    public <T> T rest(String url, Map<String, String> queryParams, Class<T> type) {

        WebTarget request = rest().path(url);
        if (queryParams != null)
            for (Map.Entry<String, String> queryParam : queryParams.entrySet())
                request = request.queryParam(queryParam.getKey(), queryParam.getValue());
        Response response = request.request(MediaType.APPLICATION_JSON)
				.get();
		if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
			throw new RuntimeException("Unexpected answer" + response);
		}
        if (type.equals(JsonNode.class)) {
            String json = response.readEntity(String.class);
            if (StringUtils.isBlank(json)) return null;
            try {
                return (T) MAPPER.readValue(json, JsonNode.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            return response.readEntity(type);
        }
    }

    public PORunner getRunner() {
        return runner;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public String getProduct() {
        return product;
    }

    public String getVariant() {
        return variant;
    }

    public String getVersion() {
        return version;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    @Override
    public String toString() {
        return String.format("TestedProduct{id %s, product %s %s %s at %s}",
                instanceId,
                product,
                variant,
                version,
                baseUrl);
    }
}
