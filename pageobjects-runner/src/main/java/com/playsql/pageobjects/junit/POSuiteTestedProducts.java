package com.playsql.pageobjects.junit;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

/**
 * Use this annotation along with {@code @RunWith(POSuiteRunner.class)}
 * to repeat a test for a set of product instances.
 *
 * For example you may want to test your plugin against:
 * Confluence Cloud deployed on port 1990,
 * Confluence 5.6 deployed on port 1991,
 * Confluence 5.7 deployed on port 1992,
 * ...
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ TYPE })
public @interface POSuiteTestedProducts {
    // The list of instances to run. Example:
    // { "confluence1,plugin", "confluence2,plugin", ... }
    String[] value();
}
