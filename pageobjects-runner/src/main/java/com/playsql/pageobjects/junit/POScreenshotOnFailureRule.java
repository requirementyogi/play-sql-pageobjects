package com.playsql.pageobjects.junit;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.PORunner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

public class POScreenshotOnFailureRule extends TestWatcher {

    protected static final Logger LOGGER = Logger.getLogger(POScreenshotOnFailureRule.class.getCanonicalName());
    private final PORunner runner;
    private final boolean active;
    private final String directory;

    public POScreenshotOnFailureRule(PORunner runner) {
        this.runner = runner;
        this.active = Boolean.valueOf(runner.getProperty("AbstractPageObjectsTest.screenshot-on-failure", null));
        this.directory = runner.getProperty("AbstractPageObjectsTest.screenshot-path", null);
    }

    @Override
    protected void failed(Throwable e, Description description) {
        if (active) {
            String name = "screenshot-" + description.getDisplayName();
            name = name.replaceAll("[^a-zA-Z0-9-\\.]", "-").replaceAll("-*$", "");
            File imgFile;
            File txtFile;
            if (directory != null) {
                File directoryFile = new File(directory);
                directoryFile.mkdirs();
                imgFile = new File(directoryFile, name + ".png");
                txtFile = new File(directoryFile, name + ".txt");
            } else {
                imgFile = new File(name + ".png");
                txtFile = new File(name + ".txt");
            }
            File file = runner.getDriver().getScreenshotAs(OutputType.FILE);
            try {
                LOGGER.info("Test " + description.getDisplayName() + " failed.\n"
                        + "file://" + imgFile.getCanonicalPath() + " is the screenshot\n"
                        + "file://" + txtFile.getCanonicalPath() + " is the stacktrace");
                FileUtils.copyFile(file, imgFile);
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
            // Also print the stacktrace to a file, so it's easier to know what when wrong
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(txtFile);
                e.printStackTrace(writer);
            } catch (FileNotFoundException e1) {
                LOGGER.info("Error while printing stacktrace into " + txtFile);
            } finally {
                IOUtils.closeQuietly(writer);
            }
        }
    }
}
