package com.playsql.pageobjects.junit;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.playsql.pageobjects.PORunner;
import com.playsql.pageobjects.TestedProduct;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import java.util.List;

/**
 * If you're not sure how to write your JUnits, please extend this class.
 * This class starts the runner before, stops it after, allows {@code @}{@link POTestedProduct} annotations
 * to exclude products on purpose.
 */
public abstract class AbstractPageObjectsTest {

    @Rule public final POIgnoreRule rule1 = new POIgnoreRule(runner);
    @Rule public final POScreenshotOnFailureRule rule2 = new POScreenshotOnFailureRule(runner);

    protected static String propertiesFile = "/pageobjects-runner.properties";
    public static PORunner runner;
    private TestedProduct defaultTestedProduct;

    /** This private member is assigned by the @POSuiteRunner annotation, if used */
    private String defaultInstances;

    /** Note: In the case of test suites, @BeforeClass and @AfterClass methods are executed once for the suite and
     * and once for each test class. There's a special case in {@link POSuiteRunner} to avoid executing the
     * the @BeforeClass and @AfterClass methods of {@link AbstractPageObjectsTest}.
     */
    @BeforeClass
    public static void beforeClassPO() {
        if (runner != null) {
            throw new IllegalStateException("Since there are static fields, this class cannot be run in parallel.");
        }
        runner = new PORunner(propertiesFile).start();
    }

	@AfterClass
	public static void afterClass() {
        runner.stop();
        runner = null;
    }

    @Before
    public void beforePO() {
        if (defaultInstances != null) {
            runner.overrideProperty("instance.default", defaultInstances);
            // Initialize instances
            product();
        }
    }

    /**
     * In situations where clients have defined the property "instance.default", this method
     * returns the first product.
     *
     * Syntax: instance.default = instanceId1, instanceId2, ...
     * As a system property: -Dpageobjects.instance.default=instanceId1,instanceId2,...
     * The other declared instanceIds are used if a PageObject of the first instance references
     * a PageObject of another product.
     *
     * @return the product. It is cached, therefore the product() method can be called often.
     */
    protected TestedProduct product() {
        if (defaultTestedProduct == null) {
            String ids = runner.getProperty("instance.default", null);
            if (ids == null) {
                throw new RuntimeException("Please specify \"instance.default = (instanceId)\" in the properties file\n" +
                        "Note that it is possible to declare several instances. Only the first one will be the " +
                        "default but the others will be used if references are made in the pageobjects.");
            }
            List<String> idList = splitInstanceIds(ids);
            if (idList.isEmpty()) {
                throw new RuntimeException("Unexpected value: instance.default = " + ids);
            }
            defaultTestedProduct = runner.getInstance(idList.get(0).trim());
            // Instantiate the other products
            for (int i = 1 ; i < idList.size() ; i++)
                runner.getInstance(idList.get(i));
        }
        return defaultTestedProduct;
    }

    public static List<String> splitInstanceIds(String instanceIds) {
        List<String> list = Lists.newArrayList();
        for (String id : instanceIds.split(",")) {
            if (id != null) {
                id = id.trim();
                if (!id.isEmpty())
                    list.add(id);
            }
        }
        return list;
    }

    public void setDefaultInstances(String defaultInstances) {
        this.defaultInstances = defaultInstances;
    }
}
