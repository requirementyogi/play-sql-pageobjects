package com.playsql.pageobjects.junit;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.MethodUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.internal.runners.statements.RunAfters;
import org.junit.internal.runners.statements.RunBefores;
import org.junit.runner.Runner;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.Suite;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;
import org.junit.runners.model.Statement;

import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 * Runs a test for each instance of the product. Please add the {@code @}{@link POSuiteTestedProducts}
 * annotation to list instances.
 */
public class POSuiteRunner extends Suite {

    protected static final Logger LOGGER = Logger.getLogger(POSuiteRunner.class.getCanonicalName());
    private Class<?> clazz;

    public POSuiteRunner(Class<?> clazz, RunnerBuilder builder) throws InitializationError {
        super(clazz, buildRunners(clazz));
        this.clazz = clazz;
    }

    // We shade @BeforeClass and @AfterClass in the suite, since they already run in the
    // children.
    protected Statement withBeforeClasses(Statement statement) {
        List<FrameworkMethod> befores = Lists.newArrayList(getTestClass().getAnnotatedMethods(BeforeClass.class));
            Iterator<FrameworkMethod> it = befores.iterator();
            while (it.hasNext()) {
                FrameworkMethod before = it.next();
                if (before.getMethod().getDeclaringClass().isAssignableFrom(AbstractPageObjectsTest.class)) {
                    it.remove();
                } else {
                    LOGGER.info("Method @BeforeClass " + before.getMethod().getName() + " in " + before.getMethod().getDeclaringClass().getSimpleName()
                            + " will be executed once for the test suite and once for each run");
                }
            }
        if (befores.isEmpty()) {
            return statement;
        } else {
            return new RunBefores(statement, befores, null);
        }
    }

    // We shade @BeforeClass and @AfterClass in the suite, since they already run in the
    // children.
    protected Statement withAfterClasses(Statement statement) {
        List<FrameworkMethod> afters = Lists.newArrayList(getTestClass().getAnnotatedMethods(AfterClass.class));
            Iterator<FrameworkMethod> it = afters.iterator();
            while (it.hasNext()) {
                FrameworkMethod after = it.next();
                if (after.getMethod().getDeclaringClass().isAssignableFrom(AbstractPageObjectsTest.class)) {
                    it.remove();
                } else {
                    LOGGER.info("Method @AfterClass " + after.getMethod().getName() + " in " + after.getMethod().getDeclaringClass().getSimpleName()
                            + " will be executed once for the test suite and once for each run");
                }
            }
        if (afters.isEmpty()) {
            return statement;
        } else {
            return new RunBefores(statement, afters, null);
        }
    }

    private static List<Runner> buildRunners(Class<?> clazz) throws InitializationError {
        List<Runner> runners = Lists.newArrayList();
        POSuiteTestedProducts annotation = clazz.getAnnotation(POSuiteTestedProducts.class);
        if (annotation == null) throw new IllegalArgumentException("Please add a @" + POSuiteTestedProducts.class.getSimpleName() + " annotation on " + clazz.getName());
        List<String> existingDefinitions = Lists.newArrayList();
        String[] testRuns = annotation.value();
        for (final String instanceIds : testRuns) {
            if (existingDefinitions.contains(instanceIds)) {
                throw new RuntimeException("There can be only one test for a set of instance IDs: \"" + instanceIds + "\" is once too many times on " + clazz.getName());
            }
            existingDefinitions.add(instanceIds);
            runners.add(new TestedProductClassRunner(clazz, instanceIds, testRuns.length > 1));
        }
        return runners;
    }

    private static class TestedProductClassRunner extends BlockJUnit4ClassRunner {
        private final String instanceIds;
        private boolean appendInstanceIds;

        /**
         * Constructor for a run of tests.
         * @param klass the test class
         * @param instanceIds the comma-separated list of instanceIds
         * @param appendInstanceIds whether or not to append the instanceIds to the test names. It's required
         *                          to append them when there are several runs, otherwise IDEA doesn't display a
         *                          hierarchical view; but it's painful to append them when there's only 1 run,
         *                          because IDEA can't run it individually.
         * @throws InitializationError if the test class is malformed, see {@link super#BlockJUnit4ClassRunner(Class)}.
         */
        public TestedProductClassRunner(Class<?> klass, String instanceIds, boolean appendInstanceIds) throws InitializationError {
            super(klass);
            this.instanceIds = instanceIds;
            this.appendInstanceIds = appendInstanceIds;
        }

        @Override
        protected Object createTest() throws Exception {
            Constructor<?> onlyConstructor = getTestClass().getOnlyConstructor();
            Object test;
            if (onlyConstructor.getParameterTypes().length == 1) {
                final List<String> list = AbstractPageObjectsTest.splitInstanceIds(instanceIds);
                test = onlyConstructor.newInstance(list);
            } else if (onlyConstructor.getParameterTypes().length == 0) {
                test = onlyConstructor.newInstance();
            } else {
                throw new RuntimeException("Illegal constructor with parameters: " + onlyConstructor.toString());
            }
            if (test instanceof AbstractPageObjectsTest) {
                ((AbstractPageObjectsTest) test).setDefaultInstances(instanceIds);
            } else {
                throw new RuntimeException("Tests with @" + POSuiteTestedProducts.class.getName() +
                        " annotation are expected to inherit from " + AbstractPageObjectsTest.class.getName());
            }
            return test;
        }

        protected void validateConstructor(List<Throwable> errors) {
            validateOnlyOneConstructor(errors);
            // Constructors must either have 0 arguments or 1 (which must be List<String>)
            //validateZeroArgConstructor(errors);
            if (errors.isEmpty()) {
                Constructor<?> onlyConstructor = getTestClass().getOnlyConstructor();
                if (!getTestClass().isANonStaticInnerClass() && !(
                        onlyConstructor.getParameterTypes().length == 0
                     || onlyConstructor.getParameterTypes().length == 1 && onlyConstructor.getParameterTypes()[0].equals(List.class)
                )) {
                    String gripe = "The constructor must only have 0 or 1 argument, of type List<String>.";
                    errors.add(new Exception(gripe));
                }
            }
        }

        public String toString() {
            return this.getClass().getSimpleName() + " {" + instanceIds + "}";
        }

        @Override
        protected String testName(FrameworkMethod method) {
            String name = super.testName(method);
            if (appendInstanceIds)
                return name + " for \"" + instanceIds + "\"";
            else
                return name;
        }

        @Override
        protected String getName() {
            String name = super.getName();
            if (appendInstanceIds)
                return name + " for \"" + instanceIds + "\"";
            else
                return name;
        }
    }
}
