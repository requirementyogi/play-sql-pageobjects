package com.playsql.pageobjects.junit;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.PORunner;
import com.playsql.pageobjects.TestedProduct;
import org.apache.commons.lang3.StringUtils;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.util.logging.Logger;

import static com.google.common.base.Objects.equal;

/**
 * This class helps you ignore tests when they don't apply to some tested products.
 * <p/>
 * For example you may choose to test one macro in Confluence 5.6 and not test it
 * in Confluence 5.7.
 * <p/>
 * Implementation note: Ignoring a test from a MethodRule doesn't seem to be possible in JUnit. If you
 * are aware of a more canonical way to perform this, please submit a pull request!
 */
public class POIgnoreRule implements MethodRule {

    protected static final Logger LOGGER = Logger.getLogger(POIgnoreRule.class.getCanonicalName());

    private final PORunner runner;

    public POIgnoreRule(PORunner runner) {
        this.runner = runner;
    }

    @Override
    public Statement apply( Statement base, FrameworkMethod method, Object target ) {
        Statement result = base;
        String reason = getReasonForIgnoring(target, method);
        if (reason != null) {
            result = new IgnoreStatement(method, target, reason);
        }
        return result;
    }

    private String getReasonForIgnoring(Object target, FrameworkMethod method) {
        POTestedProduct annotation = method.getAnnotation(POTestedProduct.class);
        if (annotation == null) return null;
        if (annotation.not().length > 0) {
            for (String instanceId : annotation.not())
                for (TestedProduct product : runner.getExistingTestedProducts())
                    if (equal(product.getInstanceId(), instanceId))
                        return "Ignored because " + instanceId + " is present";
        }
        if (annotation.value().length > 0) {
            for (String instanceId : annotation.value())
                for (TestedProduct product : runner.getExistingTestedProducts())
                    if (equal(product.getInstanceId(), instanceId))
                        return null;
            if (annotation.value().length > 1) {
                return "Ignored because none of {" + StringUtils.join(annotation.value(), ", ") + "} is present";
            } else {
                return "Ignored because " + annotation.value()[0] + " is not present";
            }
        }
        return null;
    }

    private static class IgnoreStatement extends Statement {
        private final FrameworkMethod method;
        private final Object target;
        private final String text;

        IgnoreStatement(FrameworkMethod method, Object target, String text) {
            this.method = method;
            this.target = target;
            this.text = text;
        }

        @Override
        public void evaluate() {
            LOGGER.info(method.getName() + ": " + text);
            // Assume.assumeTrue(text, false);
        }
    }
}
