package com.playsql.pageobjects;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.annotations.POWaitUntil;
import com.playsql.pageobjects.elements.AbstractPage;
import com.playsql.pageobjects.elements.IterableWebElement;
import com.playsql.pageobjects.elements.ListWebElement;
import com.playsql.pageobjects.elements.POWebElement;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.inject.Inject;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.HttpCookie;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static com.google.common.base.Objects.equal;

/**
 * Runs Selenium tests.
 *
 * <p>The runner uses a configuration file with the following properties. All are optional, except
 * instance.[name].product. This list is non-exhaustive, clients are free to add their own properties.</p>
 * <ul>
 *     <li>browser = firefox</li>
 *     <li>timeouts.default = (time in ms, default 5000ms)</li>
 *     <li>timeouts.[name] = (time in ms): The timeout for the name provided in the
 *     {@link #waitUntil(String, String)} method</li>
 *     <li>instance.[name].product : Mandatory. The product name, as can be found on {@code @}{@link POPage} annotations.
 *     Used by {@link TestedProduct}</li>
 *     <li>instance.[name].variant : the product variant (cloud, btf, customer-customized)</li>
 *     <li>instance.[name].version : PageObject classes with versions above this one will be excluded.</li>
 *     <li>instance.[name].baseurl : the base url of the product. Used for REST and browsing. URLs are automatically
 *     constructed based on this baseurl and {@code @}{@link POPage} annotations.</li>
 *     <li>instance.[name].profiles.[profile].username = admin: The login of the profile. Used by LoginPage.</li>
 *     <li>instance.[name].profiles.[profile].password = password: The password of the profile. Used by LoginPage.</li>
 *     <li>runner.screenshot-on-failure = true/false. Used by AbstractPageObjectsTest.</li>
 * </ul>
 */
public class PORunner {

    protected static final Logger LOGGER = Logger.getLogger(PORunner.class.getCanonicalName());
    protected final static String SYSTEM_PROPERTY_PREFIX = "pageobjects.";
    protected final static String PROPERTY_BROWSER = "browser";
    protected final static String BROWSER_FIREFOX = "firefox";
    protected static final String JSESSIONID_COOKIE = "JSESSIONID";
    protected static Long timeoutDefault = 5000L;

    protected final Properties configuration;
    protected RemoteWebDriver driver;
    protected String currentFrame = "";
    protected final Map<String, TestedProduct> testedProducts = Maps.newHashMap();

    /**
     * Contructs a PageObjects Runner based on the configuration file.
     * System properties such as -Dpageobjects.[key] will override the provided
     * properties.
     */
    public PORunner(String configFile) {
        this.configuration = new Properties();
        if (!StringUtils.isBlank(configFile)) {
            try {
                InputStream resourceAsStream = PORunner.class.getResourceAsStream(configFile);
                if (resourceAsStream == null) {
                    throw new RuntimeException("File not found: " + configFile);
                }
                configuration.load(resourceAsStream);
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
        applySystemPropertiesOverrides();
    }

    /**
     * Constructs a PageObjects Runner based on the provided properties.
     * System properties such as -Dpageobjects.[key] will override the provided
     * properties.
     */
    protected PORunner(Properties configuration) {
        this.configuration = configuration;
        applySystemPropertiesOverrides();
    }

    /** Merges the properties from the file and system properties */
    protected void applySystemPropertiesOverrides() {
        for (Object key : configuration.keySet()) {
            if (key instanceof String) {
                String systemOverride = System.getProperty(SYSTEM_PROPERTY_PREFIX + key);
                if (systemOverride != null) {
                    configuration.setProperty((String) key, systemOverride);
                }
            }
        }
    }

    /**
     * Starts Selenium/the browser and configure the timeouts
     * @throws IllegalStateException if the browser was already started
     **/
    public PORunner start() {
        if (driver != null) {
            throw new IllegalStateException("The browser was already started.");
        }
        String browser = configuration.getProperty(PROPERTY_BROWSER, BROWSER_FIREFOX);
        switch (browser) {
            case BROWSER_FIREFOX:
                //System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
                FirefoxOptions options = new FirefoxOptions()
                    .setProfile(new FirefoxProfile());
                driver = new FirefoxDriver(options);
                break;
            default: throw new RuntimeException("Unknown browser: " + browser);
        }

        // Configure the timeouts
        String wait = getProperty("timeouts.default", null);
        if (StringUtils.isBlank(wait)) {
            throw new RuntimeException("Timeouts must be configured: Add \"timeouts.default = 1000 #ms\" to your configuration file ");
        }
        timeoutDefault = Long.parseLong(wait);
        driver.manage().timeouts().implicitlyWait(timeoutDefault, TimeUnit.MILLISECONDS);
        return this;
    }

    /**
     * Close the browser. Please run this method in the @After method of your tests.
     * @return this
     */
    public PORunner stop() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
        return this;
    }

    /**
     * Return a product, which automatically manages baseurls and product versions.
     *
     * The products are saved in PORunner. If a pageobject contains
     * an {@link AbstractPage} field annotated with {@code @}{@link POBy}, it will lookup the
     * existing TestedProducts matching its product. This can be used to insert a "component"
     * or "subpage" or "iframe" of another product within the first one.
     *
     * @return the handler class, never null
     */
    public TestedProduct getInstance(String instance) {
        for (TestedProduct product : getExistingTestedProducts()) {
            if (equal(product.getInstanceId(), instance))
                return product;
        }
        String product = configuration.getProperty("instance." + instance + ".product", null);
        if (null == product) {
            StringBuilder sb = new StringBuilder();
            sb.append("Couldn't find a product for instanceId=").append(instance)
                    .append(". Please add the following declarations to your properties file:\n")
                    .append("instance." + instance + ".product = (product name)\n")
                    .append("instance." + instance + ".variant = \n")
                    .append("instance." + instance + ".version = \n")
                    .append("instance." + instance + ".baseurl = http://...\n");
            throw new RuntimeException(sb.toString());
        }
        TestedProduct testedProduct = new TestedProduct(
                this,
                instance,
                product,
                configuration.getProperty("instance." + instance + ".variant", null),
                configuration.getProperty("instance." + instance + ".version", null),
                configuration.getProperty("instance." + instance + ".baseurl", null)
        );
        testedProducts.put(product, testedProduct);
        return testedProduct;
    }

    public List<TestedProduct> getExistingTestedProducts() {
        return Lists.newArrayList(testedProducts.values());
    }

    /**
     * Return a product among those already configured (See {@link #getInstance(String)}).
     *
     * @param product the product name, not the instance id
     * @return
     */
    public TestedProduct getProduct(String product) {
        for (TestedProduct testedProduct : testedProducts.values())
            if (equal(testedProduct.getProduct(), product))
                return testedProduct;
        throw new RuntimeException("No product named " + product + " among: \n" + StringUtils.join(testedProducts.values(), "\n"));
    }

    /**
     * Returns a property of the configuration.
     *
     * Better use {@link TestedProduct#getProperty(String, String)}.
     *
     * @param name the name of the property
     * @param defaultValue the value, if no property with this name was provided
     * @return the value or defaultValue.
     */
    public String getProperty(String name, String defaultValue) {
        return configuration.getProperty(name, defaultValue);
    }

    public void overrideProperty(String name, String value) {
        configuration.put(name, value);
    }

    public RemoteWebDriver getDriver() {
        return driver;
    }

    /**
     * Navigate to a url.
     *
     * It's better to use a syntax like getInstance("some id").goTo(Page.class), because it will
     * lookup the right Page implementation for the product version, use the product's base url
     * and build the url from the page.
     *
     * @param url the url
     */
    public void navigateTo(String url) {
        if (driver == null) throw new IllegalStateException("Please call .start() on the PageObjectsRunner");
        driver.get(url);
    }

    /**
     * Bind the provided page to the currently displayed page. Accepted injections:
     * <ul>
     *     <li>[@POWaitUntil] @POBy WebElement: Nominal use. A webelement will be injected. If @WaitUntil is present,
     *          the injector will wait before returning until this element is displayed.</li>
     *     <li>@POBy List&lt;WebElement&gt;</li>
     *     <li>@POBy Iterable&lt;WebElement&gt;</li>
     *     <li>@POBy {@link AbstractPage}: Injects a "component" of the page,
     *      if your page is made of several components. If the component is from another product, the existing
     *      products will be used in order to find the correct page to create. Only pages with no-arg constructor
     *      must be provided.</li>
     *     <li>@Inject PageObjectsRunner</li>
     *     <li>@Inject RemoteWebDriver</li>
     *     <li>@Inject TestedProduct (if available)</li>
     * </ul>
     * @param <P> a PageObjects object, annotated by @PageObjectPage
     * @param page the object to bind to the current url
     * @param mainProduct the main product. It will be injected into the page.
     * @param scope
     * @return the {@code page} argument
     */
    public <P extends AbstractPage> P inject(P page, TestedProduct mainProduct, POBy scope) {
        List<Field> fieldsToBind = FieldUtils.getAllFieldsList(page.getClass());
        for (Field field : fieldsToBind) {
            Class<?> type = field.getType();
            Object value = null;
            POBy byAnnotation = field.getAnnotation(POBy.class);
            if (byAnnotation != null) {
                POBy mergedByAnnotation = merge(scope, byAnnotation);
                org.openqa.selenium.By annotation = toSelenium(mergedByAnnotation);
                if (type.isAssignableFrom(POWebElement.class)) {
                    value = new POWebElement(this, annotation, mergedByAnnotation.iframe());
                } else if (List.class.isAssignableFrom(type)) {
                    ListWebElement handler = new ListWebElement(driver, annotation);
                    value = Proxy.newProxyInstance(List.class.getClassLoader(), new Class[]{List.class}, handler);
                } else if (Iterable.class.isAssignableFrom(type)) {
                    value = new IterableWebElement(driver, annotation);
                } else if (AbstractPage.class.isAssignableFrom(type)) {
                    // The byAnnotation acts like a scope. Notably, iframes are checked.
                    value = TestedProduct.getInstance((Class<? extends P>) type, testedProducts.values(), mergedByAnnotation);
                } else {
                    throw new NotImplementedException("Field " + field + "annotated with @POBy, but no injectable of type " + type.getCanonicalName() + " was found");
                }
            }
            if (field.isAnnotationPresent(Inject.class)) {
                if (PORunner.class.isAssignableFrom(type)) {
                    value = this;
                } else if (RemoteWebDriver.class.isAssignableFrom(type)) {
                    value = driver;
                } else if (TestedProduct.class.isAssignableFrom(type)) {
                    value = mainProduct;
                } else {
                    throw new NotImplementedException("Field " + field + "annotated with @Inject, but no injectable of type " + type.getCanonicalName() + " was found");
                }
            }
            if (value != null) {
                try {
                    field.setAccessible(true);
                    FieldUtils.writeField(field, page, value);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        // Trigger @POWaitUntil annotations
        Class<?> clazz = page.getClass();
        boolean found = false;
        while (clazz != null) {
            for (Field field : clazz.getDeclaredFields()) {
                POWaitUntil annotation = field.getAnnotation(POWaitUntil.class);
                if (annotation != null) {
                    found = true;
                    field.setAccessible(true);
                    Object value;
                    try {
                        value = FieldUtils.readField(field, page);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                    if (value == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("package ").append(page.getClass().getPackage().getName()).append(";\n");
                        sb.append("class ").append(page.getClass().getSimpleName()).append(" {\n");
                        sb.append("    @").append(POWaitUntil.class.getSimpleName()).append(" ")
                            .append(field.getType().getName()).append(" ")
                            .append(field.getName()).append(";");
                        sb.append(" <-- This value is null ");
                        throw new RuntimeException(sb.toString());
                    } else {
                        try {
                            waitUntil(annotation.value(), null).isDisplayed((WebElement) value);
                        } catch (RuntimeException rex) {
                            StringBuilder sb = new StringBuilder(rex.getClass().getName()).append(" in:\n");
                            sb.append("package ").append(page.getClass().getPackage().getName()).append(";\n");
                            sb.append("class ").append(page.getClass().getSimpleName()).append(" {\n");
                            sb.append("    // This triggered a ").append(rex.getClass().getSimpleName()).append("\n");
                            sb.append("    @").append(POWaitUntil.class.getSimpleName())
                                    .append("\n    ").append(field.getType().getSimpleName()).append(" ")
                                    .append(field.getName()).append(" = ")
                                .append(value.toString()).append(";\n");
                            throw new RuntimeException(sb.toString(), rex);
                        }
                    }
                }
            }
            for (Method method : clazz.getMethods()) {
                found = true;
                POWaitUntil annotation = method.getAnnotation(POWaitUntil.class);
                if (annotation != null) {
                    try {
                        method.invoke(page);
                    } catch (IllegalAccessException | InvocationTargetException | RuntimeException e) {
                        throw new RuntimeException("Failure while creating " + page.getClass().getCanonicalName() + ": " +
                            "Method is @POWaitUntil " + method.toString(), e);
                    }
                }
            }
            if (!found) {
                clazz = clazz.getSuperclass();
            } else {
                clazz = null;
            }
        }

        return page;
    }

    protected static TestedProduct[] join(TestedProduct mainProduct, TestedProduct[] products) {
        if (products == null || products.length == 0) {
            return new TestedProduct[]{mainProduct};
        }
        TestedProduct[] newProducts = new TestedProduct[products.length + 1];
        newProducts[0] = mainProduct;
        for (int i = 0 ; i < products.length ; i++) {
            newProducts[i+1] = products[i];
        }
        return newProducts;
    }

    /**
     * Opens a REST client for the provided URL.
     * <p/>
     * It is better to use getInstance().rest(...) because it will manage the base url for you.
     * <p/>
     * This method manages the session cookie, so the REST call is performed under the current user's
     * name. If another authentication is required, then you don't need this method.
     *
     * @param baseUrl the base url of the REST client.
     * @return a REST client (Jersey Client)
     * @throws IllegalStateException if the browser is unauthenticated and therefore the session cookie
     * couldn't be found.
     */
    public WebTarget rest(String baseUrl) {
        final URL url;
        try {
            url = new URL(baseUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        final Set<Cookie> webDriverCookies = driver.manage().getCookies();
        Cookie webDriverCookie = Iterables.find(webDriverCookies, new Predicate<Cookie>() {
            @Override
            public boolean apply(Cookie cookie) {
                if (!equal(cookie.getName(), JSESSIONID_COOKIE)) return false;
                if (cookie.getDomain() != null && !equal(cookie.getDomain(), url.getHost())) return false;
                if (cookie.getPath() != null && !url.getPath().startsWith(cookie.getPath())) return false;
                return true;
            }
        }, null);

        final HttpCookie webDriverSessionCookie = new HttpCookie(JSESSIONID_COOKIE, webDriverCookie.getValue());
        webDriverSessionCookie.setDomain(webDriverCookie.getDomain());
        webDriverSessionCookie.setPath(webDriverCookie.getPath());

        Client client = ClientBuilder.newClient();

        if (webDriverSessionCookie == null)
            throw new IllegalStateException("Can't request the REST api if no page was displayed, because the"
                    + " session cookie is necessary.");


        client.register(new ClientRequestFilter() {
            @Override
            public void filter(ClientRequestContext context) throws IOException {
                MultivaluedMap<String, Object> headers = context.getHeaders();
                List<Object> cookieHeaders = headers.get("Cookie");
                String cookieHeader = null;
                List<HttpCookie> cookies = null;
                if (cookieHeaders != null && !cookieHeaders.isEmpty()) {
                    if (cookieHeaders.size() == 1) {
                        cookieHeader = (String) cookieHeaders.get(0);
                        if (cookieHeader != null)
                            cookies = HttpCookie.parse(cookieHeader);
                    } else {
                        throw new IllegalStateException("Can't manage a request when the \"Cookie\"" +
                                " header is multivalued");
                    }
                }
                if (cookies == null) cookies = Lists.newArrayList();
                for (HttpCookie cookie : cookies)
                    if (equal(cookie.getName(), JSESSIONID_COOKIE))
                        return;
                cookies.add(webDriverSessionCookie);
                headers.remove("Cookie");
                headers.put("Cookie", Lists.newArrayList(cookies.toArray()));
            }
        });

        return client.target(baseUrl);
    }

    protected static POBy merge(POBy... annotations) {
        if (annotations == null || annotations.length == 0) return null;
        if (annotations.length == 1) return annotations[0];
        if (annotations.length == 2 && annotations[0] == null) return annotations[1];

        StringBuilder css = new StringBuilder();
        StringBuilder iframe = new StringBuilder();
        for (POBy by : annotations) {
            if (by == null) continue;
            if (StringUtils.isNotBlank(by.value())) css.append(" ").append(by.value());
            if (StringUtils.isNotBlank(by.iframe())) iframe.append(" ").append(by.iframe());
            if (StringUtils.isNotBlank(by.linkText())
                    || StringUtils.isNotBlank(by.name())
                    || StringUtils.isNotBlank(by.partialLinkText())
                    || StringUtils.isNotBlank(by.xpath())
                    ) {
                throw new NotImplementedException("The scope css=" + css.toString() + " / iframe=" + iframe.toString() + " can't be" +
                        " used because one field has an annotation using other criteria than css/iframe: " + by);
            }
        }
        final String cssString = css.toString().trim();
        final String iframeString = iframe.toString().trim();
        return new POBy() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return POBy.class;
            }

            @Override
            public String value() {
                return cssString;
            }

            @Override
            public String linkText() {
                return null;
            }

            @Override
            public String partialLinkText() {
                return null;
            }

            @Override
            public String name() {
                return null;
            }

            @Override
            public String xpath() {
                return null;
            }

            @Override
            public String iframe() {
                return iframeString;
            }
        };
    }

    protected static By toSelenium(POBy annotation) {
        if (StringUtils.isNotBlank(annotation.value())) return org.openqa.selenium.By.cssSelector(annotation.value());
        if (StringUtils.isNotBlank(annotation.linkText())) return org.openqa.selenium.By.linkText(annotation.linkText());
        if (StringUtils.isNotBlank(annotation.partialLinkText())) return org.openqa.selenium.By.partialLinkText(annotation.partialLinkText());
        if (StringUtils.isNotBlank(annotation.xpath())) return org.openqa.selenium.By.cssSelector(annotation.xpath());
        if (StringUtils.isNotBlank(annotation.name())) return org.openqa.selenium.By.name(annotation.name());
        return By.cssSelector(annotation.value());
    }

    public interface Wait {
        Wait not();
        void isDisplayed(final WebElement webElement);
        void isDisplayed(final By selector, final String iframe);
        void isTrue(Predicate<WebDriver> predicate);
    }

    private final static int TIMEOUT_DURING_LOOP = 500;

    /**
     * Waits until an element is present.
     * @param timeoutType the type of timeout, as defined by the key
     *                    "timeouts.[type] = 10000" (time in milliseconds)
     * @return an object which handles timeout operations
     */
    public Wait waitUntil(String timeoutType, final String errorMessage) {
        String property = getProperty("timeouts." + timeoutType, null);
        long timeout;
        if (property != null)
            timeout = Long.valueOf(property);
        else
            timeout = timeoutDefault;
        // If the first findElement() waited 10 seconds, we want to be slightly above that
        timeout += 1000;
        final WebDriverWait waiter = new WebDriverWait(driver, TimeUnit.MILLISECONDS.toSeconds(timeout));
        return new Wait() {
            boolean opposite = false;
            public Wait not() {
                opposite = true;
                return this;
            }
            public void isDisplayed(final WebElement webElement) {
                driver.manage().timeouts().implicitlyWait(TIMEOUT_DURING_LOOP, TimeUnit.MILLISECONDS);
                if (webElement instanceof POWebElement) {
                    // This doesn't initialize the POWebElement, therefore it doesn't cache the result
                    isDisplayed(((POWebElement) webElement).getByAnnotation(), ((POWebElement) webElement).getIframe());
                } else {
                    // This is waiting for bugs:
                    // We don't check we're on the right iframe
                    // We haven't tested isDisplayed() works here
                    try {
                        waiter.until(webDriver -> {
                            if (opposite)
                                return !webElement.isDisplayed();
                            else
                                return webElement.isDisplayed();
                        });/*
                        waiter.until(new Predicate<WebDriver>() {
                            @Override
                            public boolean apply(WebDriver driver) {
                                if (opposite)
                                    return !webElement.isDisplayed();
                                else
                                    return webElement.isDisplayed();
                            }
                        });*/
                    } finally {
                        driver.manage().timeouts().implicitlyWait(timeoutDefault, TimeUnit.MILLISECONDS);
                    }
                }
            }
            public void isDisplayed(final By selector, final String iframe) {
                checkFrame(iframe);
                isTrue(new Predicate<WebDriver>() {
                    @Override
                    public boolean apply(WebDriver driver) {
                        try {
                            WebElement element = driver.findElement(selector);
                            return element.isDisplayed();
                        } catch (NoSuchElementException nsee) {
                            return false;
                        }
                    }

                    @Override
                    public String toString() {
                        return "Searching for element " + selector + " in iframe '" + iframe + "'";
                    }
                });
            }
            public void isTrue(Predicate<WebDriver> predicate) {
                try {
                    driver.manage().timeouts().implicitlyWait(TIMEOUT_DURING_LOOP, TimeUnit.MILLISECONDS);
                    if (opposite) {
                        waiter.until(w -> !predicate.apply(w));
                    } else {
                        waiter.until(w -> predicate.apply(w));
                    }
                } catch (RuntimeException rex) {
                    throw new RuntimeException((errorMessage != null ? errorMessage + "\n" : "") + predicate.toString(), rex);
                } finally {
                    driver.manage().timeouts().implicitlyWait(timeoutDefault, TimeUnit.MILLISECONDS);
                }
            }
        };
    }

    /**
     * Check whether we're on the right iframe. If not, switches iframe.
     * @param iframe the CSS path to the iframe
     */
    public void checkFrame(String iframe) {
        if (iframe == null) return;
        if (!equal(iframe, currentFrame)) {
            if (!currentFrame.isEmpty()) {
                LOGGER.info("Switching iframe to the default");
                driver.switchTo().defaultContent();
            }
            if (!iframe.isEmpty()) {
                LOGGER.info("Switching iframe to " + iframe);
                WebElement iframeElement = driver.findElement(By.cssSelector(iframe));
                driver.switchTo().frame(iframeElement);
            }
            currentFrame = iframe;
        }
    }

}
