package com.playsql.pageobjects.annotations;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.openqa.selenium.WebElement;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

/**
 * Marks a {@link WebElement} or a method which should be waited for.
 * The wait is performed when injecting the page object.
 *
 * If WebElement, the mention {@link POBy} is required too.
 * If method, it will wait until the method returns.
 *
 * The annotation may be present on several fields and methods; However
 * if it is present on a class, the superclass won't be considered.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD })
public @interface POWaitUntil {
    /** Timeout type, as defined by the property
     * "timeouts.[type] = 10000" (time in milliseconds)
     **/
    String value() default "";
}
