package com.playsql.pageobjects.annotations;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.PORunner;
import com.playsql.pageobjects.elements.AbstractPage;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;

/**
 * This annotation is for the package. It lists all {@link AbstractPage}s
 * of the package, so PO doesn't have to scan the package.
 *
 * The list is used by {@link PORunner} when searching for the correct version of a
 * page object.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ PACKAGE })
public @interface POPackage {
    Class<? extends AbstractPage>[] value() default {};
}
