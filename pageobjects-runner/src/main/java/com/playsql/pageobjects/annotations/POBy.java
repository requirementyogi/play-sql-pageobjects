package com.playsql.pageobjects.annotations;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.PORunner;
import org.openqa.selenium.WebElement;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * Annotates a field of type {@link WebElement},
 * which will be injected with the UI object.
 *
 * Can also be used on:<ul>
 * <li>{@code List<WebElement>}: The list is cached, therefore it contains elements of the first time it is accessed</li>
 * <li>{@code Iterable<WebElement}: This is not cached, elements are looked up every time an iterator is generated.</li>
 * <li>{@code ? extends AbstractPage}: A subpage will be injected. If an iframe is specified, all elements of the
 * subpage are assumed to be in the iframe. Elements of the AbstractPage must only use css and iframe lookups.
 * The subpage can be from a different product.
 * </li>
 *
 * @see PORunner#inject(com.playsql.pageobjects.elements.AbstractPage, com.playsql.pageobjects.TestedProduct, POBy) Other injectable objects
 * @see #iframe() Iframe management
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD })
public @interface POBy {
    /**
     * CSS selector. This is the preferred method for looking up elements.
     * Can only be null if another criteria is provided or if the field is
     * an AbstractPage.
     */
    String value();
    String linkText() default "";
    String partialLinkText() default "";
    String name() default "";
    String xpath() default "";

    /**
     * The frame where the element is located.
     *
     * <p/>Before each call, we'll check whether we're on the right iframe and switch if necessary.
     * Beware that elements without an iframe arguments are expected to be on the root iframe.
     *
     * <p/>Uses the CSS selector. Default is the root iframe.
     */
    String iframe() default "";
}
