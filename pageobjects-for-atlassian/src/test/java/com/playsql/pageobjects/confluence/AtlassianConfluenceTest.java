package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableMap;
import com.playsql.pageobjects.PORunner;
import com.playsql.pageobjects.TestedProduct;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AtlassianConfluenceTest {
    private static TestedProduct product;
	private static PORunner runner;

    @BeforeClass
    public static void setUp() {
		runner = new PORunner("/pageobjects-runner.properties").start();
		product = runner.getInstance("confluence1");
    }

	@AfterClass
	public static void closeBrowser() {
		 product.getRunner().stop();
	 }

	@Test
	public void testLogin() {
		LoginPage login = product.goTo(LoginPage.class);
        login.logoutIfNecessary();
		login.login("admin", "admin");
		String loggedInUser = product.bind(ConfluencePage.class).getLogin();
		assertThat(loggedInUser, is("admin"));
	}

	@Test
	public void testAutoLogin() {
		LoginPage page = product.goTo(LoginPage.class);
		page.login();
		String loggedInUser = page.getLogin();
		assertThat(loggedInUser, is("admin"));
	}

	private JsonNode checkDefaultSpace() {
		// If it goes through the next command, then it's authenticated
		product.rest("/rest/prototype/1/user/current", null, JsonNode.class);
		JsonNode result = product.rest("/rest/api/space", ImmutableMap.of("spaceKey", "ds"), JsonNode.class);
		JsonNode spaceListSize = result.get("size");
		if (spaceListSize.asInt() == 0) {
			// Create the space
			product.goTo(SpaceDirectoryPage.class).createSpace(null, "Demonstration Space", "ds");
		}
		return result;
	}

	@Test
	public void addMacro() {
		product.goTo(LoginPage.class).login();

		// Go to the home page
		PagePage page = product.goTo(PagePage.class, "ds", "Welcome to the Confluence Demonstration Space");
		EditorPage editor = page.edit();

		editor.insertMacro("network")
				.setMacroParam("username", "admin")
				.save();

        editor.cancel();
	}
}
