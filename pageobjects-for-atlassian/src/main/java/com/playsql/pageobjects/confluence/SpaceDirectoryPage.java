package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.POWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.inject.Inject;

@POPage(
        url = "/spacedirectory/view.action",
        product = "confluence"
)
public class SpaceDirectoryPage extends ConfluencePage {

    @POBy("#addSpaceLink")
    WebElement createSpaceButton;

    @POBy(".create-dialog-create-button")
    POWebElement nextButton;

    @POBy(".create-dialog-body")
    WebElement dialog;

    @POBy("input[name=name]")
    WebElement nameInput;

    @POBy("input[name=spaceKey]")
    WebElement keyInput;

    @Inject
    RemoteWebDriver driver;

    public void createSpace(String blueprintKey, String label, String spaceKey) {
        if (blueprintKey == null) {
            blueprintKey = "com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-space-blueprint";
        }
        createSpaceButton.click();
        // Make sure the element is available
        runner.waitUntil("long", "Waiting for blueprint")
                .isDisplayed(By.cssSelector("li[data-blueprint-module-complete-key=\"" + blueprintKey + "\"]"), null);
        Boolean result = (Boolean) driver.executeScript("return " +
                "$(\"li[data-blueprint-module-complete-key='\" + arguments[0] + \"']\")" +
                ".addClass(\"selected\")" +
                ".siblings().removeClass(\"selected\")" +
                ".size() != 0;",
                blueprintKey);
        if (result == null || !result) throw new RuntimeException("Couldn't select the blueprint");

        nextButton.click();
        runner.waitUntil("default", null).isDisplayed(nameInput);

        nameInput.clear();
        nameInput.sendKeys(label);
        keyInput.clear();
        keyInput.sendKeys(spaceKey);

        nextButton.reset();
        nextButton.click();
    }
}
