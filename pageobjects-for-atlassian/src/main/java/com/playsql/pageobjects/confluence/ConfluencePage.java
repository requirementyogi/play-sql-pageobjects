package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.annotations.POWaitUntil;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.logging.Logger;

@POPage(
    product = "confluence",
    version = "5.0"
)
public class ConfluencePage extends AbstractPage {

    protected Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

    @POWaitUntil
    @POBy(".aui-header-logo.aui-header-logo-confluence")
    protected WebElement confluenceLogo;

    @POBy("#user-menu-link")
    protected WebElement profileMenu;

    @POBy("#create-page-button")
    protected WebElement createButton;

    @POBy(".create-dialog-create-button.aui-button-primary")
    protected WebElement createDialogSubmit;

    @POBy("#aui-flag-container .aui-message")
    protected List<WebElement> auiFlags;

    public String getLogin() {
        // If not logged in, return null
        if (!profileMenu.isDisplayed()) return null;
        return profileMenu.getAttribute("data-username");
    }

    public void openCreateDialog(String blueprintKey) {
        runner.waitUntil(null, "Check the license").isDisplayed(createButton);
        createButton.click();
        runner.waitUntil("long", "Blueprint " + blueprintKey).isDisplayed(By.cssSelector("li[data-blueprint-module-complete-key=\"" + blueprintKey + "\"]"), null);
        Boolean result = (Boolean) runner.getDriver().executeScript("return " +
                "$(\"li[data-blueprint-module-complete-key='\" + arguments[0] + \"']\")" +
                ".addClass(\"selected\")" +
                ".siblings().removeClass(\"selected\")" +
                ".size() != 0;",
                blueprintKey);
        if (result == null || !result) throw new RuntimeException("Couldn't select the blueprint");

        createDialogSubmit.click();
    }

    public void dismissMessages() {
        String fatalMessage = null;
        for (WebElement flag : auiFlags) {
            String message = flag.getText().replace("\n", " – ");
            String cssClasses = flag.getAttribute("class");
            if (cssClasses != null && cssClasses.contains("aui-message-error")) {
                fatalMessage = message;
            } else {
                logger.warning("Dismissing flag: " + message);
                flag.findElement(By.cssSelector(".icon-close")).click();
            }
        }
        if (fatalMessage != null) {
            throw new RuntimeException("There was an error flag on the Confluence page: \"" + fatalMessage + "\"");
        }
    }

}
