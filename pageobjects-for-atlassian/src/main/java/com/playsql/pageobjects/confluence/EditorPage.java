package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.annotations.POWaitUntil;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@POPage(
        product = "confluence",
        version = "5.7"
)
public class EditorPage extends AbstractPage {

    private final static String EDITOR_IFRAME = "#wysiwygTextarea_ifr";
    @POWaitUntil
    @POBy("#editor-precursor")
    protected WebElement precursor;

    @POBy(value = "#tinymce", iframe = EDITOR_IFRAME)
    protected WebElement contenteditable;

    @POBy(value = "#content-title")
    protected WebElement titleField;

    @POBy(value = "#macro-browser-dialog")
    protected WebElement macroBrowser;

    @POBy(value = "#rte-button-publish")
    protected WebElement save;

    @POBy(value = "#rte-button-cancel")
    protected WebElement cancel;

    public EditorPage(String spaceKey, String title) {}
    public EditorPage(Long pageId) {}
    public EditorPage() {}

    public MacroBrowser insertMacro(String key) {

        contenteditable.clear();
        contenteditable.sendKeys("{" + key);

        By autocomplete = By.cssSelector(".autocomplete-macro-" + key);
        runner.waitUntil(null, "Waiting for autocomplete").isDisplayed(autocomplete, "");
        body.findElement(autocomplete).click();
        runner.waitUntil(null, "Waiting for macro browser").isDisplayed(macroBrowser);
        return new MacroBrowser() {
            public MacroBrowser setMacroParam(String key, String value) {
                WebElement field = macroBrowser.findElement(By.cssSelector("#macro-param-" + key));
                //field.clear();
                field.sendKeys(value);
                return this;
            }
            public void save() {
                macroBrowser.findElement(By.cssSelector(".button-panel-button.ok")).click();
            }
        };
    }

    public interface MacroBrowser {
        MacroBrowser setMacroParam(String key, String value);
        void save();
    }

    public PagePage save() {
        save.click();
        return product.bind(PagePage.class);
    }

    public PagePage cancel() {
        cancel.click();
        return product.bind(PagePage.class);
    }
}
