package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@POPage(
        product = "confluence",
        variant = "cloud",
        version = "6"
)
public class EditorCloudPage extends EditorPage {

    private static final String IFRAME = ".aui-dialog2 iframe";

    @POBy(value = "body", iframe = IFRAME)
    protected WebElement macroEditor;

    @POBy(value = ".ap-dialog-submit")
    protected WebElement macroEditorSubmit;



    public EditorCloudPage(String spaceKey, String title) {
        super(spaceKey, title);
    }

    public EditorCloudPage(Long pageId) {
        super(pageId);
    }

    public EditorCloudPage() {
    }

    @Override
    public MacroBrowser insertMacro(String key) {

        contenteditable.clear();
        contenteditable.sendKeys("{" + key);

        By autocomplete = By.cssSelector(".autocomplete-macro-" + key + "-macro");
        runner.waitUntil(null, "Waiting for autocomplete").isDisplayed(autocomplete, "");
        body.findElement(autocomplete).click();
        runner.waitUntil(null, "Waiting for macro browser").isDisplayed(macroEditor);

        return new MacroBrowser() {
            public MacroBrowser setMacroParam(String key, String value) {
                WebElement field = macroEditor.findElement(By.cssSelector("#" + key));
                //field.clear();
                field.sendKeys(value);
                field.click();
                return this;
            }
            public void save() {
                macroEditorSubmit.click();
            }
        };
    }
}
