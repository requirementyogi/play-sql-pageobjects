package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/** This is -- really -- a page of Confluence. Not an admin page, not
 * a space page, not a blog post... a Page.
 */
@POPage(
        product = "confluence"
)
public class PagePage extends AbstractPage {

    private Integer pageId;
    private String pageTitle;
    private String spaceKey;
    private String url;

    public PagePage() {
        // Default constructor
    }

    public PagePage(int pageId) {
        this.pageId = pageId;
        this.url = "/display/pages/viewpage.action?pageId=" + pageId;
    }

    public PagePage(String spaceKey, String pageTitle) {
        this.pageTitle = pageTitle;
        String escapedTitle = pageTitle.replaceAll("[^a-zA-Z0-9]", "+");
        this.url = "/display/" + spaceKey + "/" + escapedTitle;
    }

    public String getUrl() {
        return url;
    }

    @POBy("#editPageLink")
    private WebElement editButton;

    public EditorPage edit() {
        editButton.click();
        return product.bind(EditorPage.class, pageId);
    }
}
