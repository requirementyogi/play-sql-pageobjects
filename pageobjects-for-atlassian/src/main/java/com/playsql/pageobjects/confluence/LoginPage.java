package com.playsql.pageobjects.confluence;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.google.common.base.Objects;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.WebElement;

@POPage(
        product = "confluence",
        url = "/login.action?logout=true"
)
public class LoginPage extends AbstractPage {

    @POBy("#os_username")
    WebElement userField;

    @POBy("#os_password")
    WebElement passwordField;

    @POBy("#loginButton")
    WebElement submit;

    @POBy("")
    ConfluencePage base;


    /**
     * Logs in with profile sysadmin. The username and password are provided in the properties files:
     * <li> {@code instance.confluence1.profiles.sysadmin.username} </li>
     * <li> {@code instance.confluence1.profiles.sysadmin.password} </li>
     *
     */
    public void login() {
        login("sysadmin");
    }

    /**
     * Logs in as the provided profile. The usernames and passwords are provided in the properties files:
     * <li> {@code instance.confluence1.profiles.[profile name].username} </li>
     * <li> {@code instance.confluence1.profiles.[profile name].password} </li>
     */
    public void login(String profile) {
        String username = product.getProperty("profiles." + profile + ".username", "");
        String password = product.getProperty("profiles." + profile + ".password", "");
        if (Objects.equal(base.getLogin(), username)) {
            return;
        }
        this.logoutIfNecessary()
            .login(username, password);
    }

    public LoginPage logoutIfNecessary() {
        if (!userField.isDisplayed()) {
            runner.navigateTo(product.getBaseUrl() + "/logout.action");
            return product.goTo(this.getClass());
        }
        return this;
    }

    public void login(String user, String password) {
        userField.clear();
        userField.sendKeys(user);
        passwordField.clear();
        passwordField.sendKeys(user);
        submit.click();
    }

    public String getLogin() {
        return base.getLogin();
    }
}
