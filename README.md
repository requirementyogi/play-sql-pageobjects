# Status #

**Experimental**. We've been using this project in examples only. We haven't done serious testing using this framework. It means it's not ready for production-grade continuous integration. We're not sure that we've found the right modelisation of properties. We're confident that our iframe management is better than Selenium's default, and we're confident our class-level annotations are cool.

# Play SQL PageObjects #

This library extends the features of Selenium with product version management, iframes (e.g. for SAAS vs BTF deployments) and the annotated member pattern (PageFactory).

This library contains:

* **play-sql-pageobjects-runner**: The API and runtime of the pageobjects.
* **play-sql-pageobjects-for-atlassian**: A few PageObject implementations for Atlassian products.
* Notably, [pageobjects-for-atlassian/src/test/java/.../AtlassianConfluenceTest.java](pageobjects-for-atlassian/src/test/java/com/playsql/pageobjects/confluence/AtlassianConfluenceTest.java) is a good example.

It is used for testing by Play SQL's internal code.

## Why the new framework? ##

Although the implementation is agnostic, this project is tailored to the needs of testing plugins of Atlassian products. Those plugins are either deployed by customers within their product ("BTF"), or deployed in SAAS and integrated through iframes ("Atlassian Connect"). The ecosystem needs to test their plugins against several versions of the Atlassian products and against Atlassian Connect.

Here are the strengths of the framework:

* It manages **product versions**. It provides suitable implementations of Page Objects depending on the version of the product we test against.
* It manages **iframes** particularly well: One just needs to declare a class member using ```@POBy(".css-class", iframe="#tinymce") WebElement myEditor;```, and we handle ```switchTo()``` methods.
* It uses the **annotated member pattern**: It is similar to the PageFactory apprach, except I wanted more flexibility with annotations and iframes. Declaring the CSS paths on the ```@POBy()``` annotations of the class members allows better maintainability because it's separated from the logic, as opposed to writing those paths in the code.

## If you have never used Selenium / the PageObject pattern ##

When starting the command ```PORunner.with("/pageobjects-runner.properties").start();```, Selenium downloads and starts a sandboxed version of Firefox, tests a few things, then shuts down. This is perfect for an automated build server like Jenkins or Bamboo. Tests are usually defined in two independent classes: the JUnit and the PageObject. The programmer defines a PageObject class for each URL of the application. The members of this class are the UI components, defined by their CSS path (e.g. an input field). Several functions are available for WebElements, such as ```.click()``` and ```.sendKeys(text)```. The programmer defines methods for services, depending on the page's features (e.g. ```.uploadFile(filename, tags)```. The implementation of the methods use the member variables to perform their work. The PageObjects can be used by JUnit to perform tests, or by another Java program to automate an action. The PageObjects must be independent from JUnit and mustn't contain test assertions.

The major advantage of this pattern is that the implementation of the page (where elements are placed, how the layout is done) are seperated from the services of the page (uploading a file, creating a profile), so JUnits are easier to maintain. The minor advantage is that CSS paths are defined on members at the top of the PageObject, therefore they're easier to maintain even if the layout changes a little.

## Example ##

Let's add a macro on a page. Here's what the JUnit looks like:

```
#!java
	private static TestedProduct product;
	private static PORunner runner;

	@BeforeClass
	public static void setUp() {
		runner = PORunner.with("/pageobjects-runner.properties").start();
		product = runner.getProduct("confluence1");
	}

	@Test
	public void addMacro() {
		product.goTo(ConfluencePage.class).login();

		// Go to the home page
		PagePage page = product.goTo(PagePage.class, "ds", "Welcome to the Confluence Demonstration Space");
		EditorPage editor = page.edit();

		editor.insertMacro("playsql-spreadsheet")
				.setMacroParam("spreadsheetId", "Providers3")
				.save();
	}
```

Here's the Page Object:
```
#!java
@POPage(
    product = "confluence",
    version = "5.0"
)
public class ConfluencePage extends AbstractPage {

    @POWaitUntil
    @POBy(".aui-header-logo.aui-header-logo-confluence")
    protected WebElement confluenceLogo;

    @POBy("#user-menu-link")
    protected WebElement profileMenu;

    @POBy("body")
    protected WebElement body;

    @Inject
    protected TestedProduct product;

    @Inject
    protected PORunner runner;

    public String getLogin() {
        // If not logged in, return null
        if (!profileMenu.isDisplayed()) return null;
        return profileMenu.getAttribute("data-username");
    }
```

## Features ##

* Fields of a class representing a web page are automatically injected with ```WebElement```, ```List<WebElement>``` (cached), ```Iterable<WebElement>``` (non-cached), or ```AbstractPage``` for subpage components.
* Fields use a ```@POBy(css)``` annotation to provide the css selector for the field. They're loaded lazily when using the WebElement's methods.
* Fields support a ```@POWaitUntil``` annotation so the framework doesn't return a page to your JUnits until it's loaded.
* Fields support an ```@POBy(css, iframe)``` annotation, so calling the methods of a WebElement seamlessly switches iframes. If you've ever dealt with testing the WordPress or Confluence editors, you know iframe switching used to be a difficult part.
* Supports "product" annotations on pages, so you can test your product website's integration with Atlassian's Cloud platform.
* Supports "version" annotations on pages, so the framework seamlessly chooses the PageObject which suits the version of the product you're testing against, in case elements have been moved around (and this happens a lot).

### How do I get set up? ###

* Install Maven 3.0.5 or superior and Java 8
* Type ```mvn clean install``` at the root.

Use the JUnit tests to launch the developments.

### Who do I talk to? ###

* For code, just submitting a Pull Request is enough,
* Otherwise Adrien Ragot (see Play SQL website: www.play-sql.com),
* This framework is not affiliated with Atlassian.
* Talk to a counsellor if you're still disturbed with the unmatched bracket at the top of the page.