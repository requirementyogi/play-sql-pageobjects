package com.playsql.pageobjects.requirementyogi;

/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.playsql.pageobjects.annotations.POBy;
import com.playsql.pageobjects.annotations.POPage;
import com.playsql.pageobjects.confluence.ConfluencePage;
import com.playsql.pageobjects.elements.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Objects;

@POPage(url = "/requirementyogi/list.action?key=TEAM")
public class RYSearchPage extends ConfluencePage {

    @POBy("h1")
    private WebElement title;

    @POBy(".aui-nav li")
    private List<WebElement> tabs;

    public String getTitle() {
        return title.getText();
    }

    public List<String> getTabs() {
        List<String> results = Lists.newArrayList();
        for (WebElement tab : tabs) {
            results.add(tab.getText());
        }
        return results;
    }

    public RYSearchPage goToTab(String tab) {
        for (WebElement tabUI : tabs) {
            if (Objects.equals(tabUI.getText(), tab)) {
                // Click on the tab, the page reloads
                tabUI.click();
                // Ensure we've waited after the page reloads, until the selected tab is... indeed... selected
                runner
                    .waitUntil("default", "The tab wasn't selected after click")
                    .isTrue(webdriver -> Objects.equals(webdriver.findElement(By.cssSelector(".aui-nav li.aui-nav-selected")).getText(), tab));
                // Rebind all elements to a fresh pageobject
                return product.bind(RYSearchPage.class);
            }
        }
        return null;
    }
}
