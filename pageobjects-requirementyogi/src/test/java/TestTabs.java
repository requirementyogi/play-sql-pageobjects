/*
 * #%L
 * Play SQL PageObjects
 * %%
 * Copyright (C) 2015 - 2019 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.pageobjects.TestedProduct;
import com.playsql.pageobjects.confluence.LoginPage;
import com.playsql.pageobjects.junit.AbstractPageObjectsTest;
import com.playsql.pageobjects.requirementyogi.RYSearchPage;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TestTabs extends AbstractPageObjectsTest {

    @Test
    public void testTabsArePresent() {
        TestedProduct instance = runner.getInstance("confluence");
        instance.goTo(LoginPage.class).login("admin", "admin");

        RYSearchPage page = instance.goTo(RYSearchPage.class);
        assertThat(page.getTitle(), is("Confluence"));

        String[] tabs = {"Search", "Coverage", "Dependencies", "Traceability", "Diff", "Baselines", "Excel", "Import from ReqIF"};
        for (String tab : tabs) {
            page.dismissMessages();
            page = page.goToTab(tab);
            assertThat(page.getTabs(), hasItems(tabs));
        }
    }
}
