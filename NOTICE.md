
# How to contribute #

* Your commit messages must start with an issue key, such as "PO-27 Description of the changes",
* Please update the license notice at the top of the files (as described in the license),
* Please add your name or the one of your company to the list of contributors for attribution,
* Please submit the change as a pull request in BitBucket.

Thank you!

# Attribution notices #

## For the current project

Author: Adrien Ragot

Contributors:

* Be the first!

## For dependencies
